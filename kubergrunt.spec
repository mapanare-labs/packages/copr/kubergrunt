%define debug_package %{nil}

Name:           kubergrunt
Version:        0.16.0
Release:        0%{?dist}
Summary:        Kubergrunt is a standalone go binary with a collection of commands to fill in the gaps between Terraform, Helm, and Kubectl
Group:          Applications/System
License:        ASL 2.0
URL:            https://www.gruntwork.io/
Source0:        https://github.com/gruntwork-io/%{name}/archive/v%{version}.tar.gz
Source1:        https://github.com/gruntwork-io/%{name}/releases/download/v%{version}/%{name}_linux_amd64

%description
Kubergrunt is a standalone go binary with a collection 
of commands to fill in the gaps between Terraform, Helm, 
and Kubectl

%prep
%setup -q

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Apr 11 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.14.2

* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.13.1

* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.12.1

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.12.0

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.11.3

* Mon May 08 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.11.2

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.11.1

* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.10.2

* Sun Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
